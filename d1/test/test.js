const { factorial } = require('../src/util.js');
const { expect, assert } = require('chai');

// A test case or a unit test is a single description about the desired behavior of a code that either passes or fails
// Test Suites are made of collection of test cases that should be executed together. The "describe" keyword is used to group tests together.
describe('test_fun_factorials', () => {
	
	//In mocha, the describe() function is used to group tests. It accepts a string to decribes the group of tests. 

	it('test_fun_factorial_5!_is_120', () => {
		// In mocha, the it() function is used to execute individual tests. It accepts a string to describe the test and a callback function to execute assertions
		const product = factorial(5);
		expect(product).to.equal(120);

	});
})